"""
    This class will be used by GameComponents clients to create requests to the server
"""

# Module informations
__project__ = u''
__author__ = u'Pires Baptiste (baptiste.pires37@gmail.com)'
__date__ = u''
__version__ = u'1.0.0'


# Importations
from uuid import uuid4

# Specific definitions


# Classes / Functions declaration


class Request(object):
    """
    This class will be used by GameComponents clients to create requests to the server
    ---------------------------------------------------------------------------
    Attributes :
        - __id : The id of the request.
        - __type : The type of the request.
        - __params : Parameters of the request.
        - __acknowledgment : True if the request need a response.
        - __origin : Containing the socket of the one that send it.

    """

    def __init__(self,request,acknowledgment=False, params = None):
        """
        Constructor
        -----------------------------------------------------------------------
        Arguments :
         - Attributes of the request.
        -----------------------------------------------------------------------
        Return : None.
        """

        self.__id = self.generateId()
        print(self.__id)
        self.__request = request
        self.__params = params
        self.__acknowledgment = acknowledgment
        self.__origin = None

    def generateId(self):
        return uuid4()
    ## GETTERS / SETTERS ##

    def getId(self):
        return self.__id

    def getRequest(self):
        return self.__request

    def getParams(self):
        return self.__params

    def getAck(self):
        return self.__acknowledgment

    def getOrigin(self):
        return self.__origin

    def setOrigin(self, origin):
        self.__origin = origin

        
    