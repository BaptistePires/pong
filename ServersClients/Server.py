"""
    Class for #decrisption de la class
"""

# Module informations
__project__ = u''
__author__ = u'Pires Baptiste (baptiste.pires37@gmail.com)'
__date__ = u''
__version__ = u'1.0.0'


# Importations
from BasicClasses import MyBaseProcess
import socket
from ServersClients import ClientHandler, Response
from pickle import dumps
from GameComponents import LocalPlayer
# Specific definitions
REQUEST_PLAYER_CONF = "request_player_conf"
REQUEST_GET_PLAYER_POS = "request_players_pos"
REQUEST_SET_PLAYER_POS = "set_player_pos"
REQUEST_ARE_PLAYERS_HERE = "are_players_here"
REQUEST_I_AM_READY = "request_i_am_ready"
REQUEST_GET_OTHER_PLAYER_CONFIG = "get_other_player_config"
HELLO_WORLD = "hello_word"
REQUEST_A_CONFIG = "request_a_config"

# Classes / Functions declaration


class Server(MyBaseProcess.MyBaseProcess):
    """
    Class description
    ---------------------------------------------------------------------------
    Attributes :
        - __players : Array with the player of the game, containing their socket,
        position on the map and other information  TODO : Add all data contained by this att.
        - __sock : Socket used by the server.
        - __request_to_handle : Queue with all request that the server need to handle.
    
    """
    _sock = None
    _players = {}
    _are_player_here = False
    requestsToHandle = []

    def __init__(self, config):
        """
        Constructor
        -----------------------------------------------------------------------
        Arguments :
        -----------------------------------------------------------------------
        Return : None.
        """
        super(Server, self).__init__(config=config)

        # Setting up attributes
        self.__sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.__players = {}
        self.__arePlayersHere = False
        self.__requestToHandle = []

    def beforeRun(self):
        """
        This method is executed before the work of the Server
        -----------------------------------------------------------------------
        Arguments :
        -----------------------------------------------------------------------
        Return : None.
        """
        # TODO MAKE CLIENT SERVER WORKING
        # Try to bind the server
        try:
            self.__sock.bind((self._ownConfig["self_config"]["ip"], self._ownConfig["self_config"]["port"]))
            print("[+] Server binded...")
        except socket.error as exc:
            print("Socket binding error : {err}".format(err=exc.args))

        # Try to make the server listen
        try:
            self.__sock.listen(1)
            print("[+] Server listening...")
        except socket.error as exc:
            print("Socket listening error : {err}".format(err=exc.args))

        self.waitPlayers()

        for player in self.__players:
            player.start()

        self._isRunning = True



    def run(self):
        """
        Method description
        -----------------------------------------------------------------------
        Arguments :

        -----------------------------------------------------------------------
        Return :
            None

       """
        # Do tstuff before processing
        self.beforeRun()

        while self._isRunning:

            for request in self.__requestToHandle:
                if request.getRequest() == REQUEST_A_CONFIG:
                    resp = Response.Response(params=self.__players[request.getOrigin()], resp_id=request.getId())
                    print(resp.getParams())
                    serialized_resp = dumps(resp)
                    request.getOrigin().send(serialized_resp)

                self.__requestToHandle.remove(request)

        # wait the both players
        # self.__wait_players_client()

        self.afterRun()

    def afterRun(self):
        pass



    def waitPlayers(self):
        """
        This method is used to wait the both players.
        -----------------------------------------------------------------------
        Arguments :
        -----------------------------------------------------------------------
        Return : None.
        """
        player_id = 0

        while player_id < 2:

            # Accept clients
            client, addr = self.__sock.accept()
            print("Server | New client :", addr)

            new_client = ClientHandler.ClientHandler(server=self, client_addr=addr, client_sock=client, id=player_id)

            # Creating new thread for the new client
            self.__players[new_client] = LocalPlayer.LocalPlayer(config=self._ownConfig["players"][str(player_id)], id=player_id)
            # self.__players[player_id]["client_object"] = LocalPlayer.LocalPlayer(config=self._ownConfig["players"][str(player_id)], id=player_id)


            # Increments player_id to only have 2 players connected to the server
            player_id += 1


    ## GETTERS / SETTERS ##

    def addRequest(self, request):
        if not request in self.__requestToHandle:
            self.__requestToHandle.append(request)
