"""
    This class will be the Client handler
"""

# Module informations
__project__ = u''
__author__ = u'Pires Baptiste (baptiste.pires37@gmail.com'
__modifiers__ = u''
__date__ = u''
__version__ = u'1.0.0'


# Importations
from threading import Thread
from pickle import loads, PickleError
# Specific definitions


# Classes / Functions declaration


class ClientHandler(Thread):
    """
    This class will be  used whenever the server needs to create a new client.
    It will be used to handle clients requests.
    ---------------------------------------------------------------------------
    Attributes :
        - _client_sock : Socket of the client.
        - _client_addr  : Address of the client (ip, port).
        - _server      : The server object that instantiated the ClientHandler.
        - _player_id   : Id of the player that is related to the ClientHandler.
    """

    def __init__(self, server, client_sock, client_addr, id):
        """
        Constructor
        -----------------------------------------------------------------------
        Arguments :
        - client_sock : Socket of the client.
        - client_addr  : Address of the client (ip, port).
        - server      : The server object that instantiated the ClientHandler.
        - id   : Id of the player that is related to the ClientHandler.
        -----------------------------------------------------------------------
        Return : None.

        """
        # Calling mother class
        super(ClientHandler, self).__init__()

        # Setting up attributes
        self._client_sock = client_sock
        self._client_addr = client_addr
        self._server = server
        self._id = id


    def run(self):
        """
        Method called at the launch of the thread.
        -----------------------------------------------------------------------
        Arguments : None.
        -----------------------------------------------------------------------
        Return : None.
        """
        # Main loop
        while True:
            self.handleRequests()


    def handleRequests(self):
        """
        This method will handle client requests.
        -----------------------------------------------------------------------
        Arguments : None.
        -----------------------------------------------------------------------
        Return : None.
        """
        # Receive request from client
        request = self._client_sock.recv(2048)

        # Load and set up the origin of this request
        loaded_request = loads(request)
        loaded_request.setOrigin(self._client_sock)


        try:

            # Then add it to the server's requests array
            self._server.addRequest(request=loaded_request)
        except PickleError as exc:
            print("Error while loading request : {e}".format(e=exc.args))

