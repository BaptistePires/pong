"""
    Class for #decrisption de la class
"""

# Module informations
__project__ = u''
__author__ = u'Pires Baptiste (baptiste.pires37@gmail.com)'
__date__ = u''
__version__ = u'1.0.0'


# Importations


# Specific definitions


# Classes / Functions declaration


class Response(object):
    """
    Class description
    ---------------------------------------------------------------------------
    Attributes :
    
    """

    def __init__(self, params, resp_id):
        """
        Constructor
        -----------------------------------------------------------------------
        Arguments :
        -----------------------------------------------------------------------
        Return : None.
        
        """
        super(Response, self).__init__()

        # Setting up attributes
        self.__params = params
        self.__id = resp_id



    ## GETTERS / SETTERS ##

    def getId(self):
        return self.__id

    def getParams(self):
        return self.__params

    