"""
    Class for #decrisption de la class
"""

# Module informations
__project__ = u''
__author__ = u'Pires Baptiste (baptiste.pires37@gmail.com)'
__date__ = u''
__version__ = u'1.0.0'


# Importations
from BasicClasses import MyBaseProcess

import socket

from ServersClients import Request

import pickle

import time
# Specific definitions
HELLO_WORLD = "hello_word"
REQUEST_A_CONFIG = "request_a_config"


# Classes / Functions declaration


class GameClient(MyBaseProcess.MyBaseProcess):
    """
    Class description
    ---------------------------------------------------------------------------
    Attributes :
    - __sock    : Socket used to connect to the server.
    - __scene : Scene object that instantiate the GameClient
    - __are_players_here : Flag to know if both players are here.

    """

    def __init__(self, config, scene=0):
        """
        Constructor
        -----------------------------------------------------------------------
        Arguments :
        -----------------------------------------------------------------------
        Return : None.
        
        """
        super(GameClient, self).__init__(config=config)

        # Setting up attributes
        self.__sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.__scene = scene
        self.__arePlayersHere = False
        self.__isConnected = False

    def beforeRun(self):
        """
        Method called before the process starts his main loop.
        -----------------------------------------------------------------------
        Arguments : None.
        -----------------------------------------------------------------------
        Return :  None.
        """
        self.connectToTheServer()



    def run(self):

        self.beforeRun()

        while self.__isRunning:

            self.askServerForConfig()




    def helloWorld(self):

        new_request = Request.Request(request=HELLO_WORLD, acknowledgment=True)

        dumped_r = pickle.dumps(new_request)

        self.__sock.send(dumped_r)

        if new_request.getAck():
            response = self.__sock.recv(2048)
            loaded_r = pickle.loads(response)

            if loaded_r.getId() == new_request.getId():
                # print(loaded_r.getParams())
                pass


    def askServerForConfig(self):
        # Set up the request
        new_request = Request.Request(request=REQUEST_A_CONFIG, acknowledgment=True)

        # Serialize it
        serialize_request = pickle.dumps(new_request)

        try:
            # Send it
            self.__sock.send(serialize_request)
        except socket.error as exc:
            print("Error while sending request to the server : ", exc.args)
            self.join()

        # Check if ack
        if new_request.getAck():
            # Wait response
            response = self.__sock.recv(2048)

            # Load it
            deser_response = pickle.loads(response)
            print("received response ! ", deser_response)
            # Check if the request and the response matches
            if new_request.getId() == deser_response.getId():
                # print(deser_response.getParams())
                pass

    def connectToTheServer(self):
        # Try to connect to the server
        while not self.__isConnected:
            try:

                self.__sock.connect((self._ownConfig["server_ip"], self._ownConfig["server_port"]))
                print("[+] Client connected")

                self.__isConnected = True
                self.__isRunning = True


            except socket.error as exc:
                print("Error while trying to connect to the server : {err}".format(err=exc.args))


            # Try to connect to the server every 10s
            time.sleep(10)


    def isConnected(self):
        return self.__isConnected
if __name__ == '__main__':
   config = {

           "server_ip": "127.0.0.1",
           "server_port": 44444,
           "query_timeout": 0.01,
           "wait_event_timeout": 0.01

   }

   a = GameClient(config=config)

   a.start()