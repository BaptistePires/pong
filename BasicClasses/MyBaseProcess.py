"""
    Base class of all processes of the game.
"""

# Module informations
__project__ = u''
__author__ = u'Pires Baptiste (baptiste.pires37@gmail.com)'
__modifiers__ = u''
__date__ = u''
__version__ = u'1.0.0'


# Importations
# Multiprocessing library
from multiprocessing import Process, Event

# Specific definitions


# Classes / Functions declaration


class MyBaseProcess(Process):
    """
    Base class of all processes of the game.
    ---------------------------------------------------------------------------
    Attributes :
        - _own_config : Self configuration of the object
        - _is_running  : True when the process is running.
    """
    __ownConfig = {}
    __isRunning = False

    def __init__(self, config):
        """
        Constructor
        -----------------------------------------------------------------------
        Arguments : None.
        -----------------------------------------------------------------------
        Return : None.
        """

        # Initalize attributes
        self._ownConfig = config
        self._isRunning = False
        self._stopEvent = Event()

        # Calling mother class
        super(MyBaseProcess, self).__init__()


    def beforeRun(self):
        """
        Method called before processing
        -----------------------------------------------------------------------
        Arguments :

        -----------------------------------------------------------------------
        Return :
            None

        """
        pass


    def afterRun(self):
        """
        Method called before processing
        -----------------------------------------------------------------------
        Arguments : None.
        -----------------------------------------------------------------------
        Return : None.

        """
        pass
