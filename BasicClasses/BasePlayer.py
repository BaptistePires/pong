"""
    Class for #decrisption de la class
"""

# Module informations
__project__ = u''
__author__ = u'Pires Baptiste (baptiste.pires37@gmail.com)'
__date__ = u''
__version__ = u'1.0.0'


# Importations


# Specific definitions


# Classes / Functions declaration

# Importations
# My base Object
from BasicClasses import MyBaseObject

# Specific definitions


# Classes / Functions declaration

class BasePlayer(MyBaseObject.BaseObject):
    """
    Base class for all players of the game.
    ---------------------------------------------------------------------------
    Attributes :
        - _id            : id of the player.
        - __x             : x position of the player.
        - __y             : y position of the player.
        - _width         : width of the player displayed.
        - __height        : height of the player displayed.
        - __speed : Speed of the player.
    """
    def __init__(self, config):
        """
        Constructor
        -----------------------------------------------------------------------
        Arguments :
            - config : Config of the current object.
            - id     : id of the player, set in the GameComponents class.
        -----------------------------------------------------------------------
        Return : None.
        """
        # Calling mother class
        super(BasePlayer, self).__init__(config=config)
        self.__id = 0
        self.__initiated = False
        self.__x = 0
        self.__y= 0
        self.__width = 0
        self.__height = 0
        self.__speed = 0
        self.__location = ""
        self.__score = 0

    def moveLeft(self, clock_delta):
        """
        This method will be used to move the player to the left.
        -----------------------------------------------------------------------
        Arguments :
            - clock_delta : Delta of time to have the same speed of the player
            on any computer.
        -----------------------------------------------------------------------
        Return : None.
        """
        # Calculate the next position of the player
        test_x = self.__x - self.__speed * clock_delta

        # Check if he is still on the map
        if test_x >= 0:
            self.__x -= self.__speed * clock_delta

    def moveRight(self, clock_delta):
        """
        This method will be used to move the player to the right
        -----------------------------------------------------------------------
        Arguments :
            - clock_delta : Delta of time to have the same speed of the player
            on any computer.
        -----------------------------------------------------------------------
        Return : None.
        """
        # Calculate the next position of the player
        test_x = (self.__x + self.__width) + (self.__speed * clock_delta)

        # Check if he is still on the map
        if test_x < self._ownConfig["max_right"]:
            self.__x += self.__speed * clock_delta

    ## STR ##
    def __str__(self):
        returned_str = "player id     : {id} \n" \
                       "player x      : {x} \n" \
                       "player y      : {y} \n" \
                       "player width  : {width} \n" \
                       "player height : {height}".format(
            id=self.__id, x=self.__x, y=self.__y, width=self.__width, height=self.__height
        )
        return returned_str


## GETTERS | SETTERS ##

    def getScore(self):
        return self.__score

    def addScore(self, score):
        self.__score+= score

    def setLocation(self, loc):
        self.__location = loc

    def getLocation(self):
        return self.__location

    def getPlayerPos(self):
        return (self.__x, self.__y)

    def setWidth(self, width):
        self.__width = width

    def setHeight(self, height):
        self.__height = height

    def setX(self, x):
        self.__x = x

    def getX(self):
        return self.__x

    def setY(self, y):
        self.__y = y

    def getY(self):
        return self.__y

    def getId(self):
        return self.__id

    def setId(self, id):
        self.__id = id

    def setConfig(self, config):
        self._ownConfig = config

    def getWidth(self):
        return self.__width

    def getHeight(self):
        return self.__height

    def getPosAndSize(self):
        pos_size = {
            "player_id": self.__id,
            "x": self.__x,
            "y": self.__y,
            "width": self.__width,
            "height": self.__height
        }
        print(pos_size)
        return pos_size

    def setSpeed(self, speed):
        self.__speed = speed

if __name__ == '__main__':
    pass