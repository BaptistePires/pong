"""
    Class for #decrisption de la class
"""

# Module informations
__project__ = u''
__author__ = u'Pires Baptiste (baptiste.pires37@gmail.com)'
__date__ = u''
__version__ = u'1.0.0'


# Importations
from BasicClasses import MyBaseProcess

# Specific definitions


# Classes / Functions declaration

class Scene(MyBaseProcess.MyBaseProcess):
    """
    Class description
    ---------------------------------------------------------------------------
    Attributes :
        - __game : GameComponents object.
    """

    def __init__(self, config):
        """
        Constructor
        -----------------------------------------------------------------------
        Arguments :
        -----------------------------------------------------------------------
        Return : None.
        
        """
        super(Scene, self).__init__(config=config)

    def beforeRun(self):
        """
        Method description
        -----------------------------------------------------------------------
        Arguments :
        
        -----------------------------------------------------------------------
        Return :
            None

       """
        
    