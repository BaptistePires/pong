"""
    Class for #decrisption de la class
"""

# Module informations
__project__ = u''
__author__ = u'Pires Baptiste (baptiste.pires37@gmail.com)'
__date__ = u''
__version__ = u'1.0.0'

# Importations
from BasicClasses import MyBaseProcess
from GameComponents import GameView
import pygame

# Specific definitions
# GameComponents states
AVAIBLE_STATES = [
    "home",
    "waiting",
    "game",
    "multi",
    "winner"
]


# Classes / Functions declaration
class Scene(MyBaseProcess.MyBaseProcess):
    """
    Class description
    ---------------------------------------------------------------------------
    Attributes :
        - __gameView : View of the game.
        - __state : State of the game.
    """

    def __init__(self, config, main):
        """
        Constructor
        -----------------------------------------------------------------------
        Arguments :
        -----------------------------------------------------------------------
        Return : None.
        """
        super(Scene, self).__init__(config=config)

        # Setting up attributes
        self.__gameView = GameView.GameView(config=self._ownConfig["game_view"], scene=self)
        self.__state = "Home"
        self.__main = main

    def beforeRun(self):
        """
        Method description
        -----------------------------------------------------------------------
        Arguments : None.
        -----------------------------------------------------------------------
        Return : None.
        """

        self.__gameView.init()
        self._isRunning = True

    def run(self):
        self.beforeRun()

        while self._isRunning:
            self.handleEvents()
            pygame.event.get()
            self.__gameView.render(self.__state)
            self.__gameView.actions()

    def afterRun(self):
        self.join()

    def handleEvents(self):
        if self._stopEvent.is_set():
            self._isRunning = False

    def setState(self, new_state):
        if new_state.lower() in AVAIBLE_STATES:
            self.__state = new_state

    def isConnected(self):
        return self.__main.isConnected()

if __name__ == '__main__':
    c = {
        "game_view": {
            "window": {
                "width": 350,
                "height": 500
            },
            "states": {
                "home": {
                    "buttons": {
                        "button_multi": {
                            "width_divider": 1.5,
                            "height_divider": 10,
                            "x_divider": 2,
                            "y_divider": 1.2,
                            "enable_button_color": [
                                255,
                                255,
                                255
                            ],
                            "disabled_button_color": [
                                198,
                                192,
                                192
                            ],
                            "enable_label_color": [
                                0,
                                0,
                                0
                            ],
                            "disabled_label_color": [
                                66,
                                63,
                                63
                            ],
                            "label_text": "MULTIPLAYER",
                            "font_size": 27,
                            "label_font": "Berling Sans FB Demi",
                            "font_x_divider": 1.5,
                            "font_y_divider": 1.04
                        },
                        "button_local": {
                            "width_divider": 1.5,
                            "height_divider": 10,
                            "x_divider": 2,
                            "y_divider": 1.5,
                            "enable_button_color": [
                                255,
                                255,
                                255
                            ],
                            "disabled_button_color": [
                                198,
                                192,
                                192
                            ],
                            "enable_label_color": [
                                0,
                                0,
                                0
                            ],
                            "disabled_label_color": [
                                66,
                                63,
                                63
                            ],
                            "label_text": "LOCAL",
                            "font_size": 27,
                            "label_font": "Berling Sans FB Demi",
                            "font_x_divider": 1.57,
                            "font_y_divider": 1.04
                        },

                    },
                    "title": {
                    }
                },
                "local":
                    {
                        "players": {
                            "0": {
                                "pos": {
                                    "x": 150,
                                    "y": 30,
                                    "location" : "top"
                                },
                                "size": {
                                    "width": 50,
                                    "height": 10
                                },
                                "speed": 0.3,
                                "inputs": {
                                    "code_left": 97,
                                    "code_right": 100
                                },
                                "max_right": 350

                            },
                            "1": {
                                "pos": {
                                    "x": 150,
                                    "y": 470,
                                    "location": "bot"
                                },
                                "size": {
                                    "width": 50,
                                    "height": 10
                                },
                                "speed": 0.3,
                                "inputs": {
                                    "code_left": 276,
                                    "code_right": 275
                                },
                                "max_right": 350
                            }
                        },
                        "ball": {
                            "size": {
                                "width": 15,
                                "height": 15
                            },
                            "vectors": {
                                "vecX": 0,
                                "vecY": 0.3
                            }
                        },
                        "clock_tick": 60
                    }
            }
        }
    }
    a = Scene(c)
    a.start()
