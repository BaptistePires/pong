"""
    Class for #decrisption de la class
"""

# Module informations
__project__ = u''
__author__ = u'Pires Baptiste (baptiste.pires37@gmail.com)'
__date__ = u''
__version__ = u'1.0.0'


# Importations
from BasicClasses import BaseState
import pygame
import json
from os import sep
from GameComponents import Button
# Specific definitions


# Classes / Functions declaration


class WinnerState(BaseState.BaseState):
    """
    Class description
    ---------------------------------------------------------------------------
    Attributes :
    
    """

    def __init__(self,config, game_view):
        """
        Constructor
        -----------------------------------------------------------------------
        Arguments :
        -----------------------------------------------------------------------
        Return : None.
        """
        super(WinnerState, self).__init__(config=config, gameView=game_view)

        self.__winner = None
        self.__scores = []
        self.__homeButton = Button.Button(config=self._ownConfig["buttons"]["home"],parent_state=self,name="homebutton")

        self.getScores()
        self.getWinner()

    def render(self):
        self._gameView.getWindow().fill((0, 0, 0))

        self.displayWinner()
        self.displayScores()
        self.displayButton()
        pygame.display.flip()

    def actions(self):
        if self.__homeButton.isPressed(pygame.mouse.get_pos(), pygame.mouse.get_pressed()):
            self._gameView.setState("home")

    def displayWinner(self):
        """
        Method description
        -----------------------------------------------------------------------
        Arguments :
        
        -----------------------------------------------------------------------
        Return :
            None

       """
        myfont = pygame.font.SysFont(self._ownConfig["strings"]["winner"]["font"], self._ownConfig["strings"]["winner"]["size"])
        # Rendering it
        str_to_disp = self._ownConfig["strings"]["winner"]["text"].replace(self._ownConfig["strings"]["winner"]["text_to_remplace"],str(self.__winner))
        label = myfont.render(str_to_disp, 1, (255, 255, 255))
        myfont_w, myfont_h = myfont.size(str_to_disp)
        # Setting up the font pos
        title_pos = (
            (self._gameView.getWidth() - myfont_w) / 2, (self._gameView.getHeight() - myfont_h) / 5)

        # Drawing it
        self._gameView.getWindow().blit(label, title_pos)

    def displayScores(self):
        myfont = pygame.font.SysFont(self._ownConfig["strings"]["scores"]["font"],
                                     self._ownConfig["strings"]["scores"]["size"])
        # Rendering it
        str_to_disp = ""
        for n,score in enumerate(self.__scores):
            if n == 0:
                str_to_disp += str(score) +  " : "
            else:
                str_to_disp += str(score)
        label = myfont.render(str_to_disp, 1, (255, 255, 255))
        myfont_w, myfont_h = myfont.size(str_to_disp)
        # Setting up the font pos
        title_pos = (
            (self._gameView.getWidth() - myfont_w) / 2, (self._gameView.getHeight() - myfont_h) / 2)

        # Drawing it
        self._gameView.getWindow().blit(label, title_pos)

    def displayButton(self):
        pygame.draw.rect(self._gameView.getWindow(),
                         self.__homeButton.getColor(),
                         (self.__homeButton.getX(),
                          self.__homeButton.getY(),
                          self.__homeButton.getWidth(),
                          self.__homeButton.getHeight()))
        self._gameView.getWindow().blit(self.__homeButton.getLabel(),
                                        self.__homeButton.getLabelPos())
    def getScores(self):
        with open(self._ownConfig["save_data"]["path"] + sep + self._ownConfig["save_data"]["filename"]) as f:
            self.__scores = json.loads(f.read())
            f.close()

    def getWinner(self):

        highest_score = 0
        winner_id = 0
        for player_id, score in enumerate(self.__scores):
            if score > highest_score:
                highest_score = score
                winner_id = player_id

        self.__winner = winner_id