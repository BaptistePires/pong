"""
    This class will be the class for the buttons.
"""

# Module informations
__project__ = u''
__author__ = u'Pires Baptiste (baptiste.pires37@gmail.com)'
__date__ = u''
__version__ = u'1.0.0'

# Importations
from BasicClasses import MyBaseObject
import pygame


# Specific definitions


# Classes / Functions declaration


class Button(MyBaseObject.BaseObject):
    """
    Class description
    ---------------------------------------------------------------------------
    Attributes :
    
    """

    def __init__(self, config, parent_state, name):
        """
        Constructor
        -----------------------------------------------------------------------
        Arguments :
        -----------------------------------------------------------------------
        Return : None.
        
        """
        super(Button, self).__init__(config=config)

        # Setting up attributes
        self.__parentState = parent_state
        self.__size = {}
        self.__pos = {}
        self.__button_color = ()
        self.__font = None
        self.__fontSize = ()
        self.__labelPos = {}
        self.__label = None
        self.__name = name
        self.__enabled = True
        self.__label_color = []

        # Setting up those attributes
        self.loadFromConfig()

    def tick(self):
        # Setting up colors
        if self.__enabled:
            self.__button_color = self._ownConfig["enable_button_color"]
            self.__label_color = self._ownConfig["enable_label_color"]
        else:
            self.__button_color = self._ownConfig["disabled_button_color"]
            self.__label_color = self._ownConfig["disabled_label_color"]

    def loadFromConfig(self):

        # Setting up colors
        if self.__enabled:
            self.__button_color = self._ownConfig["enable_button_color"]
            self.__label_color = self._ownConfig["enable_label_color"]
        else:
            self.__button_color =self._ownConfig["disabled_button_color"]
            self.__label_color = self._ownConfig["disabled_label_color"]

        # setting up the button params
        self.__size = {
            "width": self.__parentState.getWidth() / self._ownConfig["width_divider"],

            "height": self.__parentState.getHeight() / self._ownConfig["height_divider"]
        }

        self.__pos = {
            "x": (self.__parentState.getWidth() - self.__size["width"]) / self._ownConfig["x_divider"],
            "y": (self.__parentState.getHeight() - self.__size["height"]) / self._ownConfig["y_divider"]
        }

        # Setting up button's label params
        self.__font = pygame.font.SysFont(self._ownConfig["label_font"], self._ownConfig["font_size"])

        self.__fontSize = self.__font.size(self._ownConfig["label_text"])

        self.__labelPos = {
            "x": ((self.__pos["x"] + self.__size["width"]) - self.__fontSize[0]) / self._ownConfig["font_x_divider"],
            "y": ((self.__pos["y"] + self.__size["height"]) - self.__fontSize[1]) / self._ownConfig["font_y_divider"]
        }
        print(self.__label_color)
        self.__label = self.__font.render(self._ownConfig["label_text"], 1, self.__label_color)

    def isPressed(self, mouse_pos, mouse_buttons):
        """
        This method will be used to check if the button that is displayed on the
        screen is clicked, if so, il will set an event on the scene.
        -----------------------------------------------------------------------
        Arguments :
           - mouse_pos : Position of the mouse on the screen.
           - mouse_buttons : States of the buttons of the mouse.
        -----------------------------------------------------------------------
        Return : None.
        """

        returned_value = False
        # The button can be clicked only if it's enabled
        if self.__enabled:
            # We check if the mouse's X is bigger than the X of the button
            if mouse_pos[0] > self.__pos["x"]:
                # Then we check if the mouse's X is smaller than the button's X + his width
                if mouse_pos[0] < (self.__pos["x"] + self.__size["width"]):
                    # Then we check if the mouse's Y is bigger than the Y of the button
                    if mouse_pos[1] > self.__pos["y"]:
                        # Then we check if the mouse's Y is smaller than the Y of the button + his height
                        if mouse_pos[1] < (self.__pos["y"] + self.__size["height"]):
                            if(mouse_buttons[0]):
                                returned_value = True
        else:
            returned_value = False

        return returned_value

    ## GETTERS / SETTERS ##
    def getName(self):
        return self.__name
    def getSize(self):
        return self.__size

    def getWidth(self):
        return self.__size["width"]

    def getHeight(self):
        return self.__size["height"]

    def getPos(self):
        return self.__pos

    def getX(self):
        return self.__pos["x"]

    def getY(self):
        return self.__pos["y"]

    def getColor(self):
        return self.__button_color

    def getFont(self):
        return self.__fontSize

    def getLabelPos(self):
        return (self.__labelPos["x"], self.__labelPos["y"])

    def getLabel(self):
        return self.__label

    def setEnable(self, enable):
        self.__enabled = enable
