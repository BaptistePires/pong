"""
    This will be the home state
"""

# Module informations

__project__ = u''
__author__ = u'Pires Baptiste (baptiste.pires37@gmail.com'
__modifiers__ = u''
__date__ = u''
__version__ = u'1.0.0'

# Importations
# GameState
from BasicClasses import BaseState

# import pygame
import pygame

from GameComponents import Button


# Specific definitions


# Classes / Functions declaration


class HomeState(BaseState.BaseState):
    """
    This class will be the home state
    ---------------------------------------------------------------------------
    Attributes :
        __buttons = Array of buttons on the screen.
    """

    def __init__(self, game_view, config):
        """
        Constructor
        -----------------------------------------------------------------------
        Arguments :
        -----------------------------------------------------------------------
        Return : None.

        """
        super(HomeState, self).__init__(game_view, config=config)

        # init pygame
        pygame.init()

        # Setting up attributes
        self.__buttons = []
        self.initButtons()

    def initButtons(self):
        for button in self._ownConfig["buttons"]:
            new_button = Button.Button(config=self._ownConfig["buttons"][button], parent_state=self, name=button)
            self.__buttons.append(new_button)

    def render(self):
        """
        See BaseState class for the method's description.
        -----------------------------------------------------------------------
        Arguments : None.
        -----------------------------------------------------------------------
        Return : None.
        """
        self._gameView.getWindow().fill((0, 0, 0))

        # Display the Title of this state
        self._displayTitle()
        # Display the button of this state
        for button in self.__buttons:
            # rendering the button
            pygame.draw.rect(self._gameView.getWindow(),
                             button.getColor(),
                             (button.getX(),
                                   button.getY(),
                                   button.getWidth(),
                                   button.getHeight()))
            # rendering the label of the button
            self._gameView.getWindow().blit(button.getLabel(),
                                            button.getLabelPos())
        pygame.display.flip()

    def actions(self):

        # Check if buttons are pressed
        for button in self.__buttons:
            button.tick()
            if button.isPressed(pygame.mouse.get_pos(), pygame.mouse.get_pressed()) and "local" in button.getName():
                self._gameView.setState("Game")
            elif button.isPressed(pygame.mouse.get_pos(), pygame.mouse.get_pressed()) and "multi" in button.getName():
                # self._gameView.setState("Multi")
                print("Button mutli pressed")

            if "multi" in button.getName():
                button.setEnable(self._gameView.isConnected())


    def _displayTitle(self):
        """
        This method will display the title on the screen.
        -----------------------------------------------------------------------
        Arguments : None.
        -----------------------------------------------------------------------
        Return : None.
        """
        # This method will display the GameComponents title
        # Creating the font
        myfont = pygame.font.SysFont("Berling Sans FB Demi", 90)
        # Rendering it
        label = myfont.render("PONG", 1, (255, 255, 255))
        myfont_w, myfont_h = myfont.size("PONG")
        # Setting up the font pos
        title_pos = (
            (self._gameView.getWidth() - myfont_w) / 2, (self._gameView.getHeight() - myfont_h) / 5)

        # Drawing it
        self._gameView.getWindow().blit(label, title_pos)
