"""
    Class for #decrisption de la class
"""

# Module informations
__project__ = u''
__author__ = u'Pires Baptiste (baptiste.pires37@gmail.com)'
__date__ = u'01/10/18'
__version__ = u'1.0.0'


# Importations
from BasicClasses import BaseState
from GameComponents import LocalPlayer, Ball
import pygame
from os import path,mkdir, sep
import json


# Specific definitions
LOCATION_BOT = "bot"
LOCATION_TOP = "top"

# Classes / Functions declaration


class GameState(BaseState.BaseState):
    """
    Class description
    ---------------------------------------------------------------------------
    Attributes :
    
    """ 

    def __init__(self, game_view, config):
        """
        Constructor
        -----------------------------------------------------------------------
        Arguments :
        -----------------------------------------------------------------------
        Return : None.
        """
        super(GameState, self).__init__(config=config, gameView=game_view)

        # Setting up attributes
        self.__players = []
        self.__ball = None
        self.__clock = pygame.time.Clock()


        self.init()


    def init(self):
        self.setUpPlayers()
        self.setUpBall()

    def reset(self):
        for player in self.__players:
            player.init()

        self.__ball.init()

    def setUpBall(self):
        self.__ball = Ball.Ball(config=self._ownConfig["ball"], parentState=self)
        self.__ball.init()

    def setUpPlayers(self):
        self.__players = []

        for player in self._ownConfig["players"]:
            new_player = LocalPlayer.LocalPlayer(config=self._ownConfig["players"][player], id=int(player))
            new_player.init()
            self.__players.append(new_player)

    def render(self):
        """
        Method description
        -----------------------------------------------------------------------
        Arguments :
        
        -----------------------------------------------------------------------
        Return : None
        """
        self._gameView.getWindow().fill((0, 0, 0))


        self.renderPlayers()
        self.renderBall()
        self.displayScores()
        pygame.display.flip()

    def actions(self):
        clock_delta = self.__clock.tick(self._ownConfig["clock_tick"])

        self.movePlayers(clock_delta)
        self.__ball.move(clock_delta)
        self.__ball.checkPlayersBound(self.getPlayersPosAndWidth())
        self.checkIfBallIsBehind()
        self.checkIfWinner()


    def renderBall(self):
        pygame.draw.rect(self._gameView.getWindow(),
                             (255,255,255),
                             (self.__ball.getX(),
                              self.__ball.getY(),
                              self.__ball.getWidth(),
                              self.__ball.getHeight()))


    def renderPlayers(self):
        for player in self.__players:
            pygame.draw.rect(self._gameView.getWindow(),
                             (255,255,255),
                             (player.getX(),
                              player.getY(),
                              player.getWidth(),
                              player.getHeight()))

    def checkIfWinner(self):
        winner = False
        for player in self.__players:
            if player.getScore() == 3:
                winner = True

        if winner :
            if not path.exists(self._ownConfig["save_data"]["path"]):
                mkdir(self._ownConfig["save_data"]["path"])

            players_scores = []
            for player in self.__players:
                players_scores.insert(player.getId(), player.getScore())
            data_to_save = players_scores

            with open(self._ownConfig["save_data"]["path"] + sep + self._ownConfig["save_data"]["filename"], 'w') as outfile:
                json.dump(data_to_save, outfile)
                outfile.close()
            self._gameView.setState("winner")

    def movePlayers(self, clock_delta):

        keys = pygame.key.get_pressed()
        for player in self.__players:
                if keys[player.getKeyLeft()]:
                    player.moveLeft(clock_delta)
                elif keys[player.getKeyRight()]:
                    player.moveRight(clock_delta)

    def checkIfBallIsBehind(self):

        for player in self.__players:
            if self.__ball.getY() < player.getY():
                if player.getLocation().lower() == LOCATION_TOP:
                    player.addScore(1)
                    self.reset()

            elif self.__ball.getY() > player.getY():
                if player.getLocation().lower() == LOCATION_BOT:
                    player.addScore(1)
                    self.reset()

    def getPlayersPosAndWidth(self):
        players_pos = []

        for player in self.__players:
            players_pos.append((player.getX(), player.getY(), player.getWidth()))

        return players_pos

    def displayScores(self):
        font = pygame.font.SysFont("Berlin Sans FB Demi", 20)

        for player in self.__players:
            label = font.render(str(player.getScore()), 1, (255, 255, 255))
            if player.getId() %2 == 0:
                label_pos = (self._gameView.getWidth() - 20, self._gameView.getHeight() / 2 + 15)
                # print(label_pos)
            else :
                label_pos = (self._gameView.getWidth() - 20, self._gameView.getHeight() / 2 - 15)
            self._gameView.getWindow().blit(label, label_pos)

