"""
    This class will be the class that represents the ball
"""

# Module informations
__project__ = u''
__author__ = u'Pires Baptiste (baptiste.pires37@gmail.com)'
__date__ = u'01/10/2018'
__version__ = u'1.0.0'


# Importations
from BasicClasses import MyBaseObject
import random
# Specific definitions


# Classes / Functions declaration


class Ball(MyBaseObject.BaseObject):
    """
    Class description
    ---------------------------------------------------------------------------
    Attributes :
    """

    def __init__(self, config, parentState):
        """
        Constructor
        -----------------------------------------------------------------------
        Arguments :
        -----------------------------------------------------------------------
        Return : None.
        """
        # Super class
        super(Ball, self).__init__(config=config)

        self.__parentState = parentState
        self.__x = 0
        self.__y = 0
        self.__width = 0
        self.__height = 0
        self.__speed = 0
        self.__vecX = 0
        self.__vecY = 0
        self.__launch = False

    def init(self):
        """
        Method description
        -----------------------------------------------------------------------
        Arguments : None.
        -----------------------------------------------------------------------
        Return : None.
        """
        self.setWidth(self._ownConfig["size"]["width"])
        self.setHeight(self._ownConfig["size"]["height"])
        self.setX((self.__parentState.getWidth() - self.__width) / 2)
        self.setY((self.__parentState.getHeight() - self.__height) / 2)
        self.setVecY(self._ownConfig["vectors"]["vecY"])
        self.setVecX(self._ownConfig["vectors"]["vecX"])
        self.__launch = False


    def move(self, clock_delta):
        """
        This method will be used to move the ball
        -----------------------------------------------------------------------
        Arguments :
            - clock_delta : Value used to have the smae speed no matter the computer.
        -----------------------------------------------------------------------
        Return : None.
        """
        # Check if the ball is going to be outside of hte window
        if self.__vecX <= 0:
            next_x = self.__x + (self.__vecX * clock_delta)
            if next_x <= 0:
                # If the ball's x is going to be < 0, then we invert the value of the X vector ( ex : -1 to 1)
                self.__vecX = abs(self.__vecX)
        else:
            next_x =(self.__x + self.__width) + (self.__vecX * clock_delta)
            if next_x > self.__parentState.getWidth():
                # If the ball's x is going to be < 0, then we invert the value of the Y vector ( ex : -1 to 1)
                self.__vecX = -self.__vecX

        self.__y += self.__vecY * clock_delta
        self.__x += self.__vecX * clock_delta

    def checkPlayersBound(self, players_pos_and_width):
        """
        This method will be used to check if the ball is hitting a player,
        if so the ball will react to go in a direction
        -----------------------------------------------------------------------
        Arguments :
            - player_pos_and_width : Array of Tuples with (player_x, player_y, palyer_width)
        -----------------------------------------------------------------------
        Return : None.
        """
        # Go through all players in the array
        for player in players_pos_and_width:

            # Determine the Y position of the ball, depending of its direction (if it is going up, we don't need
            # to take care of the height of the ball, but if it's going down, we need to had the height of it
            # because
            if self.__vecY > 0:
                ball_y_pos = self.getY() + self.__height
            else :
                ball_y_pos = self.getY()

            if player[1] - 5 < ball_y_pos < player[1] + 5:
                my_x_pos = self.getX()
                if self.getX() <= player[0]:
                    my_x_pos = self.getX() + self.__width

                if player[0] <= my_x_pos <= player[0] + player[2]:


                    if self.__vecY > 0:
                        self.__vecY = -self.__vecY
                    else:
                        self.__vecY = abs(self.__vecY)

                    if not self.__launch:
                        self.__vecX = -0.1
                        self.__launch = True

    ## GETTERS / SETTERS ##

    def setVecX(self, vecX):
        self.__vecX = vecX

    def setVecY(self, vecY):
        self.__vecY = vecY

    def setWidth(self, width):
        self.__width = width

    def setHeight(self, height):
        self.__height = height

    def getWidth(self):
        return self.__width

    def getHeight(self):
        return self.__height
    def setX(self, x):
        self.__x = x

    def getX(self):
        return self.__x

    def setY(self, y):
        self.__y = y

    def getY(self):
        return self.__y

    def setConfig(self, config):
        self._ownConfig = config
    def setSpeed(self, speed):
        self.__speed = speed